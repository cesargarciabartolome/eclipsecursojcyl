package es.luceit.calculador;

import static junit.framework.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import es.luceit.usuario.Alumno;
import es.luceit.usuario.AlumnoTestDataBuilder;
import es.luceit.usuario.MatriculaDeHonor;
import es.luceit.usuario.MatriculaDeHonorTestDataBuilder;

public class CalculorAyudaTest {

	private static final int RANGO_EDAD = 12;
	private static final int EDAD_MINIMA = 13;
	private static final int NUMERO_MINIMO_CREDITOS = 1;
	private static final int NUMERO_MAXIMO_CREDITOS = 8;
	private static final int NUMERO_MAXIMO_MATRICULAS = 5;
	private static final int NUMERO_MINIMO_MATRICULAS = 1;
	private static final int ANO_MINIMO_GESTIONADO = 1990;
	private static final int ANOS_CON_AYUDA = 3;
	private static final int NOTA_MINIMA_MATRICULA = 9;
	private static final int VARIACION_NOTA = 10;
	private static final int NUMERO_ALEATORIO_ALUMNOS = 25;
	private static final int NUMERO_MINIMO_ALUMNOS = 150;

	private static final String CONDICION_NOMBRE = "CAMBIADO_NOMBRE";

	private final List<Alumno> alumnos = new ArrayList<Alumno>();

	@Test
	public void aprenderALanzarTests() {
		assertTrue(alumnos.size() <= NUMERO_MINIMO_ALUMNOS
				+ NUMERO_ALEATORIO_ALUMNOS);
	}

	@Test
	public void probarCapturarExcepcionesPrimero() {
		// Solucionar esta excepción que ocurre de manera aleatoria
		for (final Alumno alumno : alumnos) {
			assertTrue(alumno.getMatriculasDeHonor().size() > 0);
		}
	}

	@Test
	public void probarBreakPointsCondicionales() {
		// Hay que saber el valor de la edad del alumno 145
		// Ponerlo en forma de "El alumno: -- tiene la edad de -- años"
		for (final Alumno alumno : alumnos) {
			assertTrue(alumno.getEdad() > 0);
		}
	}

	@Test
	public void probarCapturarExcepcionesSegundo() {
		// Solucionar esta excepción que ocurre siempre
		final CalculorAyuda calculorAyuda = new CalculorAyuda();
		final Map<Alumno, Double> alumnosResultados = calculorAyuda
				.realizarCalculo(alumnos);
		for (final Alumno alumno : alumnosResultados.keySet()) {
			assertTrue(alumnosResultados.get(alumno) > 0D);
		}
	}

	/**
	 * Inicializamos con un número de alumnos entre 15 y 20 con matriculas de
	 * honor aleatorias.
	 */
	@Before
	public void init() {
		final int numeroAleatorioDeAlumnos = NUMERO_MINIMO_ALUMNOS
				+ (int) (Math.random() * NUMERO_ALEATORIO_ALUMNOS);

		for (int i = 0; i < numeroAleatorioDeAlumnos; i++) {
			alumnos.add(generarAlumnoAleatorio(i));
		}

		modificarValoresParaDebug();
	}

	private Alumno generarAlumnoAleatorio(final int i) {
		final int edadAleatoria = EDAD_MINIMA
				+ (int) (Math.random() * RANGO_EDAD);
		final Alumno alumno = new AlumnoTestDataBuilder()
				.conNombre("Alumno" + (i + 1)).conEdad(edadAleatoria)
				.conMatriculasDeHonor(generarMatriculasAleatorias()).build();
		return alumno;
	}

	private List<MatriculaDeHonor> generarMatriculasAleatorias() {
		final List<MatriculaDeHonor> matriculasDeHonor = new ArrayList<MatriculaDeHonor>();
		final int numeroAleatorioDeMatriculas = NUMERO_MINIMO_MATRICULAS
				+ (int) (Math.random() * NUMERO_MAXIMO_MATRICULAS);
		for (int i = 0; i < numeroAleatorioDeMatriculas; i++) {
			matriculasDeHonor.add(generarMatriculaAleatoria());
		}
		return matriculasDeHonor;
	}

	private MatriculaDeHonor generarMatriculaAleatoria() {
		final int anoAleatorio = ANO_MINIMO_GESTIONADO
				+ (int) (Math.random() * ANOS_CON_AYUDA);
		final double nota = NOTA_MINIMA_MATRICULA
				+ ((int) (Math.random() * VARIACION_NOTA) / 10.0);
		final int creditosAleatorios = NUMERO_MINIMO_CREDITOS
				+ (int) (Math.random() * NUMERO_MAXIMO_CREDITOS);
		return new MatriculaDeHonorTestDataBuilder()
				.conAnoNacimiento(anoAleatorio).conNota(nota)
				.conCreditos(creditosAleatorios).build();
	}

	/**
	 * Modificamos valores para poder hacer uso de lo aprendido con el debug
	 */
	private void modificarValoresParaDebug() {
		modificarNombreAlumno();
		modificarValorMatricula();
		establecerNullAMatricula();

	}

	private Alumno obtenerAlumnoAleatoriamente() {
		final int alumnoModificado = (int) (Math.random() * alumnos.size());
		final Alumno alumnoAModificar = alumnos.get(alumnoModificado);
		return alumnoAModificar;
	}

	private void modificarValorMatricula() {
		final Alumno alumnoAModificar = obtenerAlumnoAleatoriamente();
		final int matriculaAModificar = (int) (Math.random() * alumnoAModificar
				.getMatriculasDeHonor().size()) - 1;
		final MatriculaDeHonor matriculaDeHonorAModificar = alumnoAModificar
				.getMatriculasDeHonor().get(matriculaAModificar);
		matriculaDeHonorAModificar.setCreditos(0);
	}

	private void establecerNullAMatricula() {
		final Alumno alumnoAModificar = obtenerAlumnoAleatoriamente();
		final int matriculaAModificar = (int) (Math.random() * alumnoAModificar
				.getMatriculasDeHonor().size());
		alumnoAModificar.getMatriculasDeHonor().set(matriculaAModificar, null);
	}

	private void modificarNombreAlumno() {
		final Alumno alumnoAModificar = obtenerAlumnoAleatoriamente();
		alumnoAModificar.setNombre(alumnoAModificar.getNombre()
				+ CONDICION_NOMBRE);
	}

}
