package es.luceit.atajos;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ClasesRefactorizadasTest {

	private static final double RESULTADO_FINAL = 130039.5;

	@Test
	public void testClaseRefactorizada() {
		final ClaseRefactorizada claseRefactorizada = new ClaseRefactorizada();
		claseRefactorizada.ejecucionNormal();
		assertTrue(claseRefactorizada.getResultado() == RESULTADO_FINAL);
	}

	@Test
	public void testClaseSinRefactorizar() {
		final ClaseSinRefactorizar claseSinRefactorizar = new ClaseSinRefactorizar();
		assertTrue(claseSinRefactorizar.ejecucionNormal() == RESULTADO_FINAL);
	}

}
