package es.luceit.curso;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import es.luceit.usuario.Alumno;
import es.luceit.usuario.AlumnoTestDataBuilder;

public class CursoTest {

	private static final int NUMERO_ALUMNOS_MAX = 15;
	private static final int EDAD_MINIMA = 25;
	private static final double RANGO_EDAD = 15;
	private Curso curso;
	private Date ahora;

	@Test
	public void mostrarComoBeduggear() {
		assertEquals(ahora, curso.getFecha());
	}

	@Test
	public void excepcionesSnippersYCondicionales() {
		assertTrue(curso.getAlumnos().size() > 0);
		for (final Alumno alumno : curso.getAlumnos()) {
			assertTrue(alumno.getEdad() > EDAD_MINIMA);
		}
	}

	@Before
	public void init() {
		ahora = new Date();
		final List<Alumno> alumnos = new ArrayList<Alumno>();
		for (int i = 0; i < NUMERO_ALUMNOS_MAX; i++) {
			Integer edadAleatoria = 0;
			edadAleatoria = generarEdad();
			alumnos.add(new AlumnoTestDataBuilder()
					.conNombre("Alumno" + (i + 1)).conEdad(edadAleatoria)
					.build());
		}
		curso = new CursoTestDataBuilder().conFecha(ahora).conAlumnos(alumnos)
				.conLugar("Junta de CYL").build();
	}

	private Integer generarEdad() {
		return EDAD_MINIMA + generarNumeroEntre0Y15();
	}

	private Integer generarNumeroEntre0Y15() {
		return (int) (Math.random() * RANGO_EDAD);
	}
}
