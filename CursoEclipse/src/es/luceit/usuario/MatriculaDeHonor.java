package es.luceit.usuario;

public class MatriculaDeHonor {

	protected Integer anoNacimiento;
	protected Double nota;
	protected Integer creditos;

	public Double getNota() {
		return nota;
	}

	public void setNota(final Double nota) {
		this.nota = nota;
	}

	public Integer getAnoNacimiento() {
		return anoNacimiento;
	}

	public void setAnoNacimiento(final Integer anoNacimiento) {
		this.anoNacimiento = anoNacimiento;
	}

	public Integer getCreditos() {
		return creditos;
	}

	public void setCreditos(final Integer creditos) {
		this.creditos = creditos;
	}
}
