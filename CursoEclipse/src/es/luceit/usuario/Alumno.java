package es.luceit.usuario;

import java.util.ArrayList;
import java.util.List;

public class Alumno {

	protected String nombre;
	protected Integer edad;
	protected List<MatriculaDeHonor> matriculasDeHonor = new ArrayList<MatriculaDeHonor>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(final String nombre) {
		this.nombre = nombre;
	}

	public List<MatriculaDeHonor> getMatriculasDeHonor() {
		return matriculasDeHonor;
	}

	public void setMatriculasDeHonor(
			final List<MatriculaDeHonor> matriculasDeHonor) {
		this.matriculasDeHonor = matriculasDeHonor;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(final Integer edad) {
		this.edad = edad;
	}

}
