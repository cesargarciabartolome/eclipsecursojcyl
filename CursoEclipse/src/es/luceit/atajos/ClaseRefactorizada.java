package es.luceit.atajos;

import es.luceit.usuario.Alumno;
import es.luceit.usuario.AlumnoTestDataBuilder;
import es.luceit.usuario.MatriculaDeHonor;
import es.luceit.usuario.MatriculaDeHonorTestDataBuilder;

public class ClaseRefactorizada {

	private static final String ALUMNO_INICIALIZADO = "Alumno inicializado";
	private static final double NOTA1 = 9.2D;
	private static final int CREDITOS1 = 8;
	private static final int ANO_POR_DEFECTO1 = 1990;
	private static final double NOTA2 = 9.5D;
	private static final int CREDITOS2 = 10;
	private static final int ANO_POR_DEFECTO2 = 1991;
	private static final int EDAD_ALUMNO = 25;
	private static final String NOMBRE_ALUMNO = "Juan";

	private double resultado;
	private static final ClaseRefactorizarEnumerada CLASE_ENUMERADA = ClaseRefactorizarEnumerada.ENUM2;

	public void ejecucionNormal() {
		final Alumno alumno = crearAlumno();
		final MatriculaDeHonor matriculaDeHonor1 = crearMatricula(
				ANO_POR_DEFECTO1, CREDITOS1, NOTA1);
		final MatriculaDeHonor matriculaDeHonor2 = crearMatricula(
				ANO_POR_DEFECTO2, CREDITOS2, NOTA2);
		final MatriculaDeHonor matriculaDeHonor3 = crearMatricula(
				ANO_POR_DEFECTO2, CREDITOS2, NOTA2);

		alumno.getMatriculasDeHonor().add(matriculaDeHonor1);
		alumno.getMatriculasDeHonor().add(matriculaDeHonor2);
		alumno.getMatriculasDeHonor().add(matriculaDeHonor3);

		System.out.println(ALUMNO_INICIALIZADO);

		resultado = calcularElResultado(alumno);
		incrementarResultado();

	}

	private void incrementarResultado() {
		switch (CLASE_ENUMERADA) {
		case ENUM1:
			resultado += 1;
			break;
		case ENUM10:
			resultado += 10;
			break;
		case ENUM11:
			resultado += 11;
			break;
		case ENUM12:
			resultado += 12;
			break;
		case ENUM2:
			resultado += 2;
			break;
		case ENUM3:
			resultado += 3;
			break;
		case ENUM4:
			resultado += 4;
			break;
		case ENUM5:
			resultado += 5;
			break;
		case ENUM6:
			resultado += 6;
			break;
		case ENUM7:
			resultado += 7;
			break;
		case ENUM8:
			resultado += 8;
			break;
		case ENUM9:
			resultado += 9;
			break;

		}
	}

	private Double calcularElResultado(final Alumno alumno) {
		Double resultado = 0D;
		for (final MatriculaDeHonor matriculaDeHonor : alumno
				.getMatriculasDeHonor()) {
			resultado += resultado
					+ ((matriculaDeHonor.getNota() * matriculaDeHonor
							.getAnoNacimiento()) + matriculaDeHonor
							.getCreditos());
		}
		return resultado;
	}

	private MatriculaDeHonor crearMatricula(final int ano, final int creditos,
			final double nota) {
		return new MatriculaDeHonorTestDataBuilder().conAnoNacimiento(ano)
				.conCreditos(creditos).conNota(nota).build();
	}

	private Alumno crearAlumno() {
		return new AlumnoTestDataBuilder().conNombre(NOMBRE_ALUMNO)
				.conEdad(EDAD_ALUMNO).build();
	}

	public double getResultado() {
		return resultado;
	}

	public void setResultado(final double resultado) {
		this.resultado = resultado;
	}

}
