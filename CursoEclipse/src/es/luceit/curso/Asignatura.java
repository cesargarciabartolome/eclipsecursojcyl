package es.luceit.curso;

public enum Asignatura {
	INGLES("Inglés"), MATEMATICAS("Matematicas"), LENGUAJE("Lenguaje"), FISICA(
			"Física"), QUIMICA("Quimica"), RELIGION("Religión"), ETICA("Ética"), GIMNASIA(
			"Gimnasia");

	public String nombre;

	Asignatura(final String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

}
