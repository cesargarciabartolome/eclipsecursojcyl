package es.luceit.usuario;

import java.util.ArrayList;
import java.util.List;

import es.luceit.databuilder.TestDataBuilder;
import es.luceit.usuario.Alumno;
import es.luceit.usuario.MatriculaDeHonor;

public class AlumnoTestDataBuilder<ATDB extends AlumnoTestDataBuilder<ATDB>>
		implements TestDataBuilder<Alumno> {

	private String nombre;
	private Integer edad;
	private List<MatriculaDeHonor> matriculasDeHonor = new ArrayList<MatriculaDeHonor>();

	@Override
	public Alumno build() {
		final Alumno alumno = new Alumno();
		alumno.nombre = nombre;
		alumno.matriculasDeHonor = matriculasDeHonor;
		alumno.edad = edad;
		return alumno;
	}

	public ATDB conNombre(final String nombre) {
		this.nombre = nombre;
		return (ATDB) this;
	}

	public ATDB conEdad(final Integer edad) {
		this.edad = edad;
		return (ATDB) this;
	}

	public ATDB conMatriculasDeHonor(
			final List<MatriculaDeHonor> matriculasDeHonor) {
		this.matriculasDeHonor = matriculasDeHonor;
		return (ATDB) this;
	}

}
