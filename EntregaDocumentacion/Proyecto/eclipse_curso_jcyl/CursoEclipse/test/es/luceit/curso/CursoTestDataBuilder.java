package es.luceit.curso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.luceit.databuilder.TestDataBuilder;
import es.luceit.usuario.Alumno;

public class CursoTestDataBuilder<CTDB extends CursoTestDataBuilder<CTDB>>
		implements TestDataBuilder<Curso> {

	private Date fecha;
	private String lugar;
	private List<Alumno> alumnos = new ArrayList<Alumno>();

	@Override
	public Curso build() {
		final Curso curso = new Curso();
		curso.fecha = fecha;
		curso.lugar = lugar;
		curso.alumnos = alumnos;
		return curso;
	}

	public CTDB conFecha(final Date fecha) {
		this.fecha = fecha;
		return (CTDB) this;
	}

	public CTDB conLugar(final String lugar) {
		this.lugar = lugar;
		return (CTDB) this;
	}

	public CTDB conAlumnos(final List<Alumno> alumnos) {
		this.alumnos = alumnos;
		return (CTDB) this;
	}

}
