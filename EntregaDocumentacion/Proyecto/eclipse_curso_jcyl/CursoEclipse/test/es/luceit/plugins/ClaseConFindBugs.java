package es.luceit.plugins;

public class ClaseConFindBugs {

	private final Integer variableEstaticaNula = null;

	public void nullPointer() {
		if (variableEstaticaNula > 0) {
		}
	}

	public void asignacioneDeParametros(String cadenaParametro) {
		cadenaParametro = "Nueva cadena";
	}

	public void imposibleCast() {
		final Object doubleValue = Double.valueOf(1.0);
		final Long value = (Long) doubleValue;
		System.out.println("   - " + value);

	}
}
