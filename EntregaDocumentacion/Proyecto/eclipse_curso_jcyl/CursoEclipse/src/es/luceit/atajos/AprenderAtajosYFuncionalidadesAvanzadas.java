package es.luceit.atajos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.luceit.curso.Asignatura;
import es.luceit.usuario.Alumno;

public class AprenderAtajosYFuncionalidadesAvanzadas {

	private static final String CADENA3 = "Cadena3";
	private static final String CADENA2 = "Cadena2";
	private static final String CADENA1 = "Cadena1";
	// Getter y Setter
	private String nombre;
	private String apellido;

	public void aprederAManejarEclipseMejoraNuestraVida() {

		inicializarLista();

		// Local variable
		final Integer resultado = getMultiplicacion(5, 12);

		// Autosugerido System.out.println() ;
		System.out.println();
		// CamelCase con ArrayIndexOutOfBoundsException

		// Plantilla switch
		// plantilla switch de enumerado Asignatura
		// Convertir a if-else
		final Asignatura asignatura = null;

		// Plantilla con: if , for, for con lista , switch
		final Alumno alumno = new Alumno();
		final List<Alumno> alumnos = new ArrayList<Alumno>();

		// Autocompletado mejorado
		alumno.getMatriculasDeHonor().get(0).getCreditos();

		final Map<Alumno, String> mapa = new HashMap<Alumno, String>();
		for (final Map.Entry<Alumno, String> entry : mapa.entrySet()) {
			final Alumno key = entry.getKey();
			final String value = entry.getValue();

		}
	}

	public Integer getMultiplicacion(final Integer val1, final Integer val2) {
		return val1 * val2;

	}

	/**
	 * Método creado para JCYL
	 */
	private void inicializarLista() {
		// Extraer a método
		// Extraer a constrante
		final List<String> listaDeCadenas = new ArrayList<String>();
		listaDeCadenas.add(CADENA1);
		listaDeCadenas.add(CADENA2);
		listaDeCadenas.add(CADENA3);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(final String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(final String apellido) {
		this.apellido = apellido;
	}

}
