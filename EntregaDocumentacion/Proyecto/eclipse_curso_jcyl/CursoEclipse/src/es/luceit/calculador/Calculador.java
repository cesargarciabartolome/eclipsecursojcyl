package es.luceit.calculador;

import java.util.List;
import java.util.Map;

import es.luceit.usuario.Alumno;

/**
 * 
 * Interface para calculadores
 * 
 * @author cesar.garcia
 * 
 */
public interface Calculador {

	/**
	 * Interface común para todos los calculadores
	 * 
	 * @param alumnos
	 *            Alumnos implicados en el cálculo
	 * 
	 * @return El resultado del cálculo disgregado por alumno. Se devuelve de
	 *         manera que cada alumno es la clave para poder acceder a su valor.
	 */
	Map<Alumno, Double> realizarCalculo(final List<Alumno> alumnos);
}
