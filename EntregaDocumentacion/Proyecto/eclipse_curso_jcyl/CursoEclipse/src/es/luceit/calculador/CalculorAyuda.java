package es.luceit.calculador;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.luceit.usuario.Alumno;
import es.luceit.usuario.MatriculaDeHonor;

public class CalculorAyuda implements Calculador {

	private static final Double PORCENTAJE_DIFICULTAD = 0.2;

	@Override
	public Map<Alumno, Double> realizarCalculo(final List<Alumno> alumnos) {
		final Map<Alumno, Double> alumnosResultados = new HashMap<Alumno, Double>();
		for (final Alumno alumno : alumnos) {
			Double resultadoTotal = 0D;
			for (final MatriculaDeHonor matriculaDeHonor : alumno
					.getMatriculasDeHonor()) {
				final Double resultadoParcial = (matriculaDeHonor.getNota() / matriculaDeHonor
						.getCreditos())
						+ (alumno.getEdad() * PORCENTAJE_DIFICULTAD);
				resultadoTotal += resultadoParcial;
			}
			alumnosResultados.put(alumno, resultadoTotal);
		}
		return alumnosResultados;
	}

}
