package es.luceit.curso;

import java.util.Date;
import java.util.List;

import es.luceit.usuario.Alumno;

public class Curso {

	protected Date fecha;
	protected String lugar;
	protected List<Alumno> alumnos;

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(final Date fecha) {
		this.fecha = fecha;
	}

	public List<Alumno> getAlumnos() {
		return alumnos;
	}

	public void setAlumnos(final List<Alumno> alumnos) {
		this.alumnos = alumnos;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(final String lugar) {
		this.lugar = lugar;
	}
}
