package es.luceit.atajos;

public enum ClaseRefactorizarEnumerada {

	ENUM1("Enum1"), ENUM2("Enum2"), ENUM3("Enum3"), ENUM4("Enum4"), ENUM5(
			"Enum5"), ENUM6("Enum6"), ENUM7("Enum7"), ENUM8("Enum8"), ENUM9(
			"Enum9"), ENUM10("Enum10"), ENUM11("Enum11"), ENUM12("Enum12");

	public String codigo;

	ClaseRefactorizarEnumerada(final String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}
}
