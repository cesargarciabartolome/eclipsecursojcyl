package es.luceit.atajos;

import es.luceit.usuario.Alumno;
import es.luceit.usuario.AlumnoTestDataBuilder;
import es.luceit.usuario.MatriculaDeHonor;
import es.luceit.usuario.MatriculaDeHonorTestDataBuilder;

public class ClaseSinRefactorizar {

	public Double ejecucionNormal() {
		final Alumno alumno = new AlumnoTestDataBuilder().conNombre("Juan")
				.conEdad(25).build();
		final MatriculaDeHonor matriculaDeHonor1 = new MatriculaDeHonorTestDataBuilder()
				.conAnoNacimiento(1990).conCreditos(8).conNota(9.2D).build();

		// Descomentar y mover código???
		// alumno.getMatriculasDeHonor().add(matriculaDeHonor2);

		// final MatriculaDeHonor matriculaDeHonor2 = crearMatricula(
		// ANO_POR_DEFECTO2, CREDITOS2, NOTA2);

		alumno.getMatriculasDeHonor().add(matriculaDeHonor1);

		// matriculaDeHonor3 igual que la matriculaDeHonor2 y añadirla a alumno

		// Imprimir por consola

		Double resultado = 0D;
		for (int i = 0; i < alumno.getMatriculasDeHonor().size(); i++) {
			resultado = resultado
					+ ((alumno.getMatriculasDeHonor().get(i).getNota() * alumno
							.getMatriculasDeHonor().get(i).getAnoNacimiento()) + alumno
							.getMatriculasDeHonor().get(i).getCreditos());
		}

		final ClaseRefactorizarEnumerada claseRefactorizarEnumerada = ClaseRefactorizarEnumerada.ENUM2;
		if (claseRefactorizarEnumerada == ClaseRefactorizarEnumerada.ENUM1) {
			resultado += 1;
		} else if (claseRefactorizarEnumerada == ClaseRefactorizarEnumerada.ENUM10) {
			resultado += 10;
		} else if (claseRefactorizarEnumerada == ClaseRefactorizarEnumerada.ENUM11) {
			resultado += 11;
		} else if (claseRefactorizarEnumerada == ClaseRefactorizarEnumerada.ENUM12) {
			resultado += 12;
		} else if (claseRefactorizarEnumerada == ClaseRefactorizarEnumerada.ENUM2) {
			resultado += 2;
		} else if (claseRefactorizarEnumerada == ClaseRefactorizarEnumerada.ENUM3) {
			resultado += 3;
		} else if (claseRefactorizarEnumerada == ClaseRefactorizarEnumerada.ENUM4) {
			resultado += 4;
		} else if (claseRefactorizarEnumerada == ClaseRefactorizarEnumerada.ENUM5) {
			resultado += 5;
		} else if (claseRefactorizarEnumerada == ClaseRefactorizarEnumerada.ENUM6) {
			resultado += 6;
		} else if (claseRefactorizarEnumerada == ClaseRefactorizarEnumerada.ENUM7) {
			resultado += 7;
		} else if (claseRefactorizarEnumerada == ClaseRefactorizarEnumerada.ENUM8) {
			resultado += 8;
		} else if (claseRefactorizarEnumerada == ClaseRefactorizarEnumerada.ENUM9) {
			resultado += 9;
		}

		return resultado;

	}

}
