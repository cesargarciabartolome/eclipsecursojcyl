package es.luceit.atajos;

import java.util.ArrayList;
import java.util.List;

import es.luceit.curso.Asignatura;
import es.luceit.usuario.Alumno;

public class AprenderAtajosYFuncionalidadesAvanzadas {

	// Getter y Setter
	private String nombre;
	private String apellido;

	public void aprederAManejarEclipseMejoraNuestraVida() {

		// Extraer a método
		// Extraer a constrante
		final List<String> listaDeCadenas = new ArrayList<String>();
		listaDeCadenas.add("Cadena1");
		listaDeCadenas.add("Cadena2");
		listaDeCadenas.add("Cadena3");

		// Local variable
		getMultiplicacion(5, 12);

		// Autosugerido System.out.println() ;
		// CamelCase con ArrayIndexOutOfBoundsException

		// Plantilla switch
		// plantilla switch de enumerado Asignatura
		// Convertir a if-else
		final Asignatura asignatura = null;

		// Plantilla con: if , for, for con lista , switch
		final Alumno alumno = new Alumno();
		final List<Alumno> alumnos = new ArrayList<Alumno>();

		// Autocompletado mejorado
		alumno.getMatriculasDeHonor().get(0).getAnoNacimiento();
	}

	public Integer getMultiplicacion(final Integer val1, final Integer val2) {
		return val1 * val2;

	}
}
