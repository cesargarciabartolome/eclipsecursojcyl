package es.luceit.usuario;

import es.luceit.databuilder.TestDataBuilder;
import es.luceit.usuario.MatriculaDeHonor;

public class MatriculaDeHonorTestDataBuilder<MDHTDB extends MatriculaDeHonorTestDataBuilder<MDHTDB>>
		implements TestDataBuilder<MatriculaDeHonor> {

	private Integer anoNacimiento;
	private Double nota;
	private Integer creditos;

	@Override
	public MatriculaDeHonor build() {
		final MatriculaDeHonor matriculaDeHonor = new MatriculaDeHonor();
		matriculaDeHonor.anoNacimiento = anoNacimiento;
		matriculaDeHonor.nota = nota;
		matriculaDeHonor.creditos = creditos;
		return matriculaDeHonor;
	}

	public MDHTDB conAnoNacimiento(final Integer anoNacimiento) {
		this.anoNacimiento = anoNacimiento;
		return (MDHTDB) this;
	}

	public MDHTDB conNota(final Double nota) {
		this.nota = nota;
		return (MDHTDB) this;
	}

	public MDHTDB conCreditos(final Integer creditos) {
		this.creditos = creditos;
		return (MDHTDB) this;
	}
}
