package es.luceit.plugins;

public class ClaseConPMD {

	// Variable estática a nula
	private final Integer variableEstaticaNula = null;

	public Integer ifVacio() {
		Integer valor = 0;
		if (variableEstaticaNula > 0) {
			valor = variableEstaticaNula;
			valor++;
		} else {

		}
		return valor;
	}

	public void ifQueSePuedenUnir() {
		if (variableEstaticaNula > 0) {
			if (variableEstaticaNula > 5) {
				System.out.println("Curso aprobado");
			}
		}
	}

	public void malUsoDeString() {
		String cadenaDeSalidaDePantalla = "El nombre del Alumno:";
		cadenaDeSalidaDePantalla += " Juan ";
		cadenaDeSalidaDePantalla += " . Tiene una nota final de: ";
		cadenaDeSalidaDePantalla += " 6.5";
	}

	public class Foo {
		Boolean bar = true;
		Boolean buz = false;
		final String cadena7890123456789 = "";
	}

}
