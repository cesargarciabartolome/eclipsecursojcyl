package es.luceit.plugins;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClaseConCheckStyle {

	public void lineaDeMasDe80Caracteres() {
		final Map<String, List<String>> lineaDeMasDe80Caracteres = new HashMap<String, List<String>>();
		lineaDeMasDe80Caracteres.clear();
	}

	public void numeroMagicos() {
		Integer valorInteger = 25;
		valorInteger += 1;
		Double valorDouble = 55.4;
		valorDouble += 1.2;
	}

	public String innerAssignment() {
		String resultado = "[";
		return resultado += "]";
	}

	public void asignacioneDeParametros(String cadenaParametro) {
		cadenaParametro = "Nueva cadena";
	}

	public void comparacionConNulo() {
		final String cadena = null;
		if (cadena.equals("CadenaAComparar")) {

		}
	}

	public void metodoDeMasDe4Parametros(final String paramatro1,
			final Integer paramatro2, final Float paramatro3,
			final Double paramatro4, final List<String> paramatro5) {
	}

	public void ifElseAnidados(final Double valor) {
		if (valor < 0) {
			if (valor > 5) {
				System.out.println("Superado");
			}
		} else {
			System.out.println("Valor erroneo");
		}

	}

	public void bloqueVacio(final Double valor) {
		if (valor > 1) {
		} else {
			System.out.println("Valor erroneo");
		}

	}

	public void metodoDeMasDe30Lineas() {
		Integer valor1 = 1;
		Integer valor2 = 1;
		Integer valor3 = 1;
		Integer valor4 = 1;
		Integer valor5 = 1;
		Integer valor6 = 1;
		Integer valor7 = 1;
		Integer valor8 = 1;
		Integer valor9 = 1;
		Integer valor10 = 1;
		Integer valor11 = 1;
		Integer valor12 = 1;
		Integer valor13 = 1;
		Integer valor14 = 1;
		Integer valor15 = 1;

		valor1 += 1;
		valor2 += 1;
		valor3 += 1;
		valor4 += 1;
		valor5 += 1;
		valor6 += 1;
		valor7 += 1;
		valor8 += 1;
		valor9 += 1;
		valor10 += 1;
		valor11 += 1;
		valor12 += 1;
		valor13 += 1;
		valor14 += 1;
		valor15 += 1;
	}

}
