package es.luceit.databuilder;

/**
 * Patrón TestDataBuilder, tal y como se cuenta aquí:
 * http://www.natpryce.com/articles/000714.html
 * 
 * 
 * @param <T>
 */
public interface TestDataBuilder<T> {

	T build();
}